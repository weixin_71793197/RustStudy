use std::io;
use rand::Rng;
use std::cmp::Ordering;
fn main() {
    let range_number = rand::thread_rng().gen_range(1..101);

    println!("猜数游戏 Number Guessing Game");
    println!("猜一个数 Guess a number");

    let mut guess = String::new();
    io::stdin().read_line(&mut guess).expect("无法读取行 Could not read the line");

    let guess:u32 = guess.trim().parse().expect("请输入一个数字 Please enter a number");

    println!("你猜测的数是The number you guessed is:{}", guess);

    match guess.cmp(&range_number){
        Ordering::Less => println!("太小了 Too small"),
        Ordering::Greater => println!("太大了 Too big"),
        Ordering::Equal => println!("你赢了 You win"),
    }

    println!("神秘数字是 The secret number is: {}", range_number);
}