use std::io;
use rand::Rng;
fn main() {
    let range_number = rand::thread_rng().gen_range(1..101);

    println!("猜数游戏 Number Guessing Game");

    println!("猜一个数 Guess a number");

    let mut guess = String::new();

    io::stdin().read_line(&mut guess).expect("无法读取行 Could not read the line");

    println!("你猜测的数是The number you guessed is:{}", guess);

    println!("神秘数字是 The secret number is: {}", range_number);
}